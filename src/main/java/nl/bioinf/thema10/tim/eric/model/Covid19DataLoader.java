package nl.bioinf.thema10.tim.eric.model;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.*;

public class Covid19DataLoader {

    /**
     * Reads in COVID data, creates cases objects from this data, puts them into Hashmaps and adds these to a
     * WrapperClass object and returns it.
     * @param covidData InputStream of COVID19 data filed
     * */
    public WrapperClass readCovidData(InputStream covidData) throws IOException {
        // Set Empty HashMaps
        HashMap<String, List<Case>> casesWorldWide = new HashMap<>();
        HashMap<String, List<Case>> casesDutchProvinces = new HashMap<>();
        HashMap<String, List<Case>> casesAllCountries = new HashMap<>();
        // Add worldwide to its hashmap
        casesWorldWide.put("Worldwide", new ArrayList<>());

        // Create populationDataLoader
        PopulationDataLoader populationDataLoader = new PopulationDataLoader();
        // Get data streams
        InputStream popData = populationDataLoader.getClass().getResourceAsStream("/population.csv");
        InputStream popDataProv = populationDataLoader.getClass().getResourceAsStream("/province_population.csv");
        //Generate population HashMaps
        HashMap<String, Long> popMap = populationDataLoader.readPopulationData(popData);
        HashMap<String, Long> popMapProvinces = populationDataLoader.readPopulationData(popDataProv);

        // Read covidData
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(covidData));
        try {
            String header = reader.readLine(); // Not used
            while (reader.ready()) {
                line = reader.readLine();
                if (!line.isEmpty()) {
                    //split comma separated line
                    String[] items = line.split(",", -1);
                    //parse to hasmaps
                    if (items[12].equals("Worldwide")){
                        // If statement filters Worlwide data
                        // Create and add Case object
                        Case newCase = createCaseFromItems(items);
                        casesWorldWide.get("Worldwide").add(newCase);
                    }

                   else if (items[12].equals("Netherlands") && !(items[13].isEmpty()) && items[14].isEmpty()) {
                        // If statement filters Dutch province data
                        // Create Case object
                        Case newCase = createCaseFromItems(items);
                        // Get province name
                        String province = items[13];
                        // Add province if it's not in the Hasmap already
                        if (!casesDutchProvinces.containsKey(province)) {
                            casesDutchProvinces.put(province, new ArrayList<>());
                        }
                        // Get population from province
                        long population = popMapProvinces.getOrDefault(province.toUpperCase(), (long)10000000);

                        // Go to next line if confirmed Change is a correction
                        if (newCase.confirmedChange < 0) {
                            continue;
                        }

                        // Calculate and set confirmed change per 100.000 inhabitants
                        newCase.setConfirmedChangePerPop((float) Math.abs(newCase.confirmedChange * 100000 / population));
                        // Set province
                        newCase.setProvince(items[13]);
                        // Add newCases to cases from province
                        casesDutchProvinces.get(province).add(newCase);
                    }

                    else if (items[13].isEmpty()) {
                        // If statement checks if AdminRegion value is empty, to select countrywide data only
                        // Get Case object
                        Case newCase = createCaseFromItems(items);
                        // Get country name
                        String country = items[12];
                        // Add country if it's not in the Hashmap already
                        if (!casesAllCountries.containsKey(country)) {
                            casesAllCountries.put(country, new ArrayList<>());
                        }
                        // Get population from country
                        long population = popMap.getOrDefault(country.toUpperCase(), (long) 10000000);

                        // Go to next line if confirmed Change is a correction
                        if (newCase.confirmedChange < 0){
                            continue;
                        }

                        // Calculate and set confirmed change per 100.000 inhabitants
                        newCase.setConfirmedChangePerPop((float) Math.abs(newCase.confirmedChange * 100000 / population));
                        // Add newCases to cases from country
                        casesAllCountries.get(country).add(newCase);
                    }
                }
            }

        } catch (ArrayIndexOutOfBoundsException|NumberFormatException e) {
            System.out.println(line);
            e.printStackTrace();
        } finally {
            reader.close();
        }
        // Add maps to data structure
        return new WrapperClass(casesWorldWide, casesDutchProvinces, casesAllCountries);
    }

    /**
     * Creates a Case object from a String array of items found in each line of the COVID19 Data
     * Should be used within a file reading while loop.
     * @param items String array of items: ID, Updated, Confirmed, ConfirmedChange, ect.
     * @return new Case object.
     * */
    public Case createCaseFromItems(String[] items){
        return new Case(
                !items[0].isEmpty() ? Long.parseLong(items[0]) : 0, // ID
                !items[1].isEmpty() ? items[1] : "-", // Updated
                !items[2].isEmpty() ? Integer.parseInt(items[2]) : 0, // Confirmed
                !items[3].isEmpty() ? Integer.parseInt(items[3]) : 0, // ConfirmedChange
                !items[4].isEmpty() ? Integer.parseInt(items[4]) : 0, // Deaths
                !items[5].isEmpty() ? Integer.parseInt(items[5]) : 0, // DeathsChange
                !items[6].isEmpty() ? Integer.parseInt(items[6]) : 0, // Recovered
                !items[7].isEmpty() ? Integer.parseInt(items[7]) : 0, // RecoveredChange
                !items[8].isEmpty() ? Float.parseFloat(items[8]) : 0, // Latitude
                !items[9].isEmpty() ? Float.parseFloat(items[9]) : 0 // Longitude
        );
    }

    /**
     * Writes a List of Case objects to JSON String
     * @param cases Input list of cases
     * @return gson.toJson() function to turn data into a Json string
     * */
    public String casesToJSON(List<Case> cases){
        // Create GsonBuilder
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                // This will include any null-values in the JSON output
                .serializeNulls()
                .create();
        // Write cases to Json string
        return gson.toJson(cases);
    }

    /**
     * Writes WrapperClass to JSON String
     * @param data Input WrapperClass
     * @return gson.toJson() function to turn data into a Json string
     * */
    public String dataToJSON(WrapperClass data){
        // Create GsonBuilder
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                // This will include any null-values in the JSON output
                .serializeNulls()
                .create();
        // Write data to Json string
        return gson.toJson(data);
    }
}