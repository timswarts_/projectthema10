package nl.bioinf.thema10.tim.eric.model;

public class Case {

    long id;
    String updated;
    int confirmed;
    int confirmedChange;
    int deaths;
    int deathsChange;
    int recovered;
    int recoveredChange;
    float lat;
    float lon;
    float confirmedChangePerPop;
    String province = null;

    public Case(long id, String updated, int confirmed, int confirmedChange, int deaths, int deathsChange, int recovered, int recoveredChange, float lat, float lon) {
        this.id = id;
        this.updated = updated;
        this.confirmed = confirmed;
        this.confirmedChange = confirmedChange;
        this.deaths = deaths;
        this.deathsChange = deathsChange;
        this.recovered = recovered;
        this.recoveredChange = recoveredChange;
        this.lat = lat;
        this.lon = lon;
    }

    // Note, these are all optional getters
    public long getId() {
        return id;
    }

    public String getUpdated() {
        return updated;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public int getConfirmedChange() {
        return confirmedChange;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getDeathsChange() {
        return deathsChange;
    }

    public int getRecovered() {
        return recovered;
    }

    public int getRecoveredChange() {
        return recoveredChange;
    }

    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }

    // Setters for confirmedChangePerPop and Province
    public void setConfirmedChangePerPop(float value){this.confirmedChangePerPop = value;}

    public void setProvince(String province){
        this.province = province;
    }

    @Override
    public String toString() {
        return "Case{" +
                "id=" + id +
                ", updated='" + updated + '\'' +
                ", confirmed=" + confirmed +
                ", confirmedChange=" + confirmedChange +
                ", deaths=" + deaths +
                ", deathsChange=" + deathsChange +
                ", recovered=" + recovered +
                ", recoveredChange=" + recoveredChange +
                ", lat=" + lat +
                ", lon=" + lon +
                ", confirmedChangePerPop=" + confirmedChangePerPop +
                '}';
    }
}
