package nl.bioinf.thema10.tim.eric.servlets;

import nl.bioinf.thema10.tim.eric.model.Covid19DataLoader;
import nl.bioinf.thema10.tim.eric.model.WrapperClass;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

@WebServlet(name = "CovidDataServlet", urlPatterns = "/dataparsing")
public class CovidDataServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Create data loader
        Covid19DataLoader dataLoader = new Covid19DataLoader();
        // Create InputStream of complete dump of COVID cases, located in the src/main/resources folder
        InputStream covidData = dataLoader.getClass().getResourceAsStream("/Bing-COVID19-Data.csv");
        // Read and parse the COVID data
        WrapperClass cases = dataLoader.readCovidData(covidData);
        // Create json response
        String json = dataLoader.dataToJSON(cases);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
