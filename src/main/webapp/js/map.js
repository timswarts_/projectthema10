require([
    "esri/Map",
    "esri/widgets/Editor",
    "esri/views/MapView",
    "esri/request",
    "esri/layers/FeatureLayer",
    "esri/widgets/TimeSlider",
    "esri/smartMapping/popup/templates",
    "esri/widgets/Legend",
    "esri/widgets/Search"
], function(Map, Editor, MapView, esriRequest, FeatureLayer, TimeSlider, popupTemplateCreator, Legend, Search) {
    let layerView, previousFeatureObjIDs;

    let map = new Map({
        basemap: "dark-gray-vector",
    });

    let view = new MapView({
        container: "viewDiv",
        map: map,
        center: [10.451526,  51.165691],
        zoom: 3
    });


    let template = {
        title: "Confirmed cases per 100.000 inhabitants",
        content: [
            {
                type: "fields",
                fieldInfos: [
                    {
                        fieldName: "confirmedChangePerPop",
                        label: "new cases per 100 thousand inhabitants"
                    },
                    {
                        fieldName: "date",
                        label: "date"
                    },
                    {
                        fieldName: "confirmed",
                        label: "total confirmed cases"
                    },
                ]
            }
        ]
    };

    let renderer = {
        type: "simple",
        symbol: {
            type: "simple-marker"
        },
        visualVariables: [{
            type: "color",
            field: "confirmedChangePerPop",
            stops: [
                { value: 0, color: "rgba(255,255,255,0.8)" },
                // Risk levels based on the Dutch corona dashboard
                { value: 5, color: "rgba(250,173,173,0.8)" },
                { value: 50, color: "rgba(255, 102, 102, 0.8)" },
                { value: 150, color: "rgba(255, 0, 0, 0.8)" },
                { value: 250, color: "rgba(153,0,0,0.8)" },
                // Added extra colour
                { value: 300, color: "rgba(73 ,0 ,0, 0.8)" }
            ]
        },
            {
                type: "size",
                field: "pointSize",
                stops: [
                    { value: 0, size: 8 },
                    { value: 1, size: 14 },
                ]
            }]
    }

    let options = {
        query: {
            limit: 30
        },
        responseType: "json"
    };

    let features = [];
    let objectID = 1;
    let dates = [];

    /**
     * Perform a GET request for cases data in JSON format.
     * Data is received as a list of features where each feature is an cases event. This data is
     * reformatted in the ArcGIS feature-format, combined into a FeatureLayer which is then added
     * to the existing map object for visualization.
     */


    esriRequest("/dataparsing", options).then(function(response) {
        // Get values from response
        const {countries} = response.data;
        const {netherlands} = response.data;
        const {worldwide} = response.data;

        // Loop through country names
        let countryCases = Object.keys(countries);
        countryCases.forEach(country => {
            countries[country].forEach(newCase => {
                // Get values from case object
                let pointSize = 1;
                const {confirmedChangePerPop} = newCase;
                const {lat} = newCase;
                const {lon} = newCase;
                const {updated} = newCase;
                const {confirmed} = newCase;
                const {deaths} = newCase;
                const {deathsChange} = newCase;

                // Set geometry
                let geometry = {
                    type: "point",
                    x: parseFloat(lon),
                    y: parseFloat(lat),
                };

                // Parse the date to a JS date object
                let date = new Date(updated);
                dates.push({
                    original: updated,
                    converted: date,
                    formatted: date.toLocaleString()
                });

                // Set attributes
                let attr = {
                    ObjectID: objectID,
                    confirmedChangePerPop: confirmedChangePerPop,
                    country: country,
                    date: date.getTime(),
                    confirmed: confirmed,
                    deaths:deaths,
                    deathsChange:deathsChange,
                    pointSize:pointSize
                };

                // Push features
                features.push({
                    geometry: geometry,
                    attributes: attr,
                });
                objectID++;
            });

        });

        // Configure country popups
        const popupCases = {
            "title": "{country}",
            "content": [{
                "type": "fields",
                "fieldInfos": [
                    {
                        "fieldName": "confirmedChangePerPop",
                        "label": "Confirmed change per 100.000 inhabitants",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },
                    {
                        "fieldName": "confirmed",
                        "label": "Total cases",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },
                    {
                        "fieldName": "date",
                        "label": "Date",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },

                    {
                        "fieldName": "deaths",
                        "label": "Total deaths",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },

                    {
                        "fieldName": "deathsChange",
                        "label": "Change in death",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },
                ]
            }]
        }

        // Create FeatureLayer
        let dutchCases = new FeatureLayer({
            source: features,
            objectIdField: "ObjectID",
            renderer: renderer,
            template: template,
            popupTemplate: popupCases,
            title: "test",
            timeInfo: {
                startField: "date", // name of the date field
                interval: { // specify time interval for
                    unit: "months",
                    value: 1
                }
            },
            fields: [
                {
                    name: "ObjectID",
                    type: "oid"
                },{
                    name: "confirmedChangePerPop",
                    type: "double",
                },{
                    name: "country",
                    type: "string"
                },{
                    name: "date",
                    type: "date"
                },{
                    name: "confirmed",
                    type: "double"
                },
                {
                    name: "deaths",
                    type: "double"
                },
                {
                    name: "deathsChange",
                    type: "double"
                },
                {
                    name: "pointSize",
                    type: "integer"
                }
            ]
        });

        // Add layer to map
        map.add(dutchCases);

        // Repeat above for provinces
        let pointSize = 0;
        let dutchProvinceCases = Object.keys(netherlands);
        dutchProvinceCases.forEach(province => {
            netherlands[province].forEach(newCase => {
                // Get values from Case object
                let pointSize = 0;
                const {confirmedChangePerPop} = newCase;
                const {lat} = newCase;
                const {lon} = newCase;
                const {updated} = newCase;
                const {confirmed} = newCase;
                const {deaths} = newCase;
                const {deathsChange} = newCase;
                const {province} = newCase;

                // Set geometry
                let geometry = {
                    type: "point",
                    x: parseFloat(lon),
                    y: parseFloat(lat),
                };

                // Parse the date to a JS date object
                let date = new Date(updated);
                dates.push({
                    original: updated,
                    converted: date,
                    formatted: date.toLocaleString()
                });

                // Set attributes
                let attr = {
                    ObjectID: objectID,
                    confirmedChangePerPop: confirmedChangePerPop,
                    Country: "Netherlands",
                    date: date.getTime(),
                    confirmed: confirmed,
                    deaths: deaths,
                    deathsChange: deathsChange,
                    province: province,
                    pointSize: pointSize
                };

                // push features
                features.push({
                    geometry: geometry,
                    attributes: attr,
                });

                objectID++;
            });

        });

        // Configure province popups
        let provincePopup = {
            "title": "{province}",
            "content": [{
                "type": "fields",
                "fieldInfos": [
                    {
                        "fieldName": "confirmedChangePerPop",
                        "label": "Confirmed change per 100.000 inhabitants",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },
                    {
                        "fieldName": "confirmed",
                        "label": "Total cases",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },
                    {
                        "fieldName": "date",
                        "label": "Date",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },

                    {
                        "fieldName": "deaths",
                        "label": "Total deaths",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },

                    {
                        "fieldName": "deathsChange",
                        "label": "Change in death",
                        "isEditable": true,
                        "tooltip": "",
                        "visible": true,
                        "format": null,
                        "stringFieldOption": "text-box"
                    },
                ]
            }]
        }

        // Create province feature layers
        let provinceCases = new FeatureLayer({
            source: features,
            objectIdField: "ObjectID",
            renderer: renderer,
            template: template,
            popupTemplate: provincePopup,
            title: "test",
            timeInfo: {
                startField: "date", // name of the date field
                interval: { // specify time interval for
                    unit: "months",
                    value: 1
                }
            },
            fields: [
                {
                    name: "ObjectID",
                    type: "oid"
                },{
                    name: "confirmedChangePerPop",
                    type: "double",
                },{
                    name: "country",
                    type: "string"
                },{
                    name: "date",
                    type: "date"
                },{
                    name: "confirmed",
                    type: "double"
                },
                {
                    name: "deaths",
                    type: "double"
                },
                {
                    name: "deathsChange",
                    type: "double"
                },
                {
                    name: "province",
                    type: "string"
                },
                {
                    name: "pointSize",
                    type: "integer"
                }
            ]
        });

        // Add to map
        map.add(provinceCases);

        // create a new TimeSlider widget
        const timeSlider = new TimeSlider({
            container: "timeSlider",
            view: view,
            mode: "cumulative-from-start",
            playRate: 25,
            stops: {
                interval: {
                    value: 1,
                    unit: "days"
                }
            }
        });

        view.ui.add(timeSlider, "bottom-leading");

        view.whenLayerView(dutchCases)
            .then(function(lv) {
                // The layerview for the layer
                layerView = lv;

                // console.log(dutchCases.timeInfo)
                const start = dutchCases.timeInfo.fullTimeExtent.start;

                // Set start- and end-dates to the range provided by the data
                timeSlider.fullTimeExtent = {
                    start: start,
                    end: dutchCases.timeInfo.fullTimeExtent.end
                };
            });

        timeSlider.watch("timeExtent", function () {
            // update layer view filter to reflect current timeExtent
            layerView.effect = {
                filter: {
                    timeExtent: timeSlider.timeExtent,
                    geometry: view.extent
                },
                // excludedEffect: "grayscale(20%) opacity(12%)"
            };
        });

        // Create legend
        let legend = new Legend({
            view: view,
            container: "covidLegend",
            layerInfos: [{
                layer: dutchCases,
                title: "New cases per 100.000 inhabitants",
                label: "Cases per 100.000 inhabitants",
            }]
        });

        view.ui.add(legend, "bottom-right");

        const searchWidget = new Search({
            view: view,
        });

        // Adds the search widget below other elements in
        // the top left corner of the view
        view.ui.add(searchWidget, {
            position: "top-right",
            index: 2
        });



    })})