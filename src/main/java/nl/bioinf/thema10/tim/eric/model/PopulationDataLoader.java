package nl.bioinf.thema10.tim.eric.model;

import java.io.*;
import java.util.HashMap;

public class PopulationDataLoader {

    /**
     * Read through population data and return a HashMap of population data per region
     * @param popReader InputStream of population data file
     * @return populationPerRegion HashMap with region name as key and population as value
     * */
    public HashMap<String, Long> readPopulationData(InputStream popReader) throws IOException {
        // Empty HashMap
        HashMap<String, Long> populationPerRegion = new HashMap<>();
        // Read file
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(popReader));
        try {
            while (reader.ready()) {
                line = reader.readLine();
                // Get items
                String[] items = line.split(",");
                String value = items[1];
                // Put to HashMap
                populationPerRegion.put(items[0].toUpperCase(), Long.parseLong(value.stripLeading()));
            }
        } catch (ArrayIndexOutOfBoundsException|NumberFormatException e) {
            System.out.println(line);
            e.printStackTrace();
        } finally {
            reader.close();
        }
        return populationPerRegion;
    }
}

