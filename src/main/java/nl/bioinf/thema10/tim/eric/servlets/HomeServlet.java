package nl.bioinf.thema10.tim.eric.servlets;

import nl.bioinf.thema10.tim.eric.config.WebConfig;
import nl.bioinf.thema10.tim.eric.model.randomNumberGenerator;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

@WebServlet(name = "HomeServlet", urlPatterns = "/home", loadOnStartup = 1)
public class HomeServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.getTemplateEngine();
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        final String number = randomNumberGenerator.getNumberAsString();
        ctx.setVariable("random_number", number);

        WebConfig.getTemplateEngine().process("home", ctx, response.getWriter());
    }
}
