package nl.bioinf.thema10.tim.eric.model;
import java.util.concurrent.ThreadLocalRandom;

public class randomNumberGenerator {
    public static String getNumberAsString(){
        int x = ThreadLocalRandom.current().nextInt(0, 10001);
        return Integer.toString(x);
    }
}
