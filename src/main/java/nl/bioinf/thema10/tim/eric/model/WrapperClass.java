package nl.bioinf.thema10.tim.eric.model;

import java.util.HashMap;
import java.util.List;

public class WrapperClass {
    HashMap<String, List<Case>> world;
    HashMap<String, List<Case>> netherlands;
    HashMap<String, List<Case>> countries;

    public WrapperClass(HashMap<String, List<Case>> world, HashMap<String, List<Case>> netherlands,
                        HashMap<String, List<Case>> countries){
        this.world = world;
        this.netherlands = netherlands;
        this.countries = countries;
    }

    HashMap<String, List<Case>> getWorld(){ return this.world; }
    HashMap<String, List<Case>> getNetherlands(){ return this.netherlands; }
    HashMap<String, List<Case>> getCountries(){ return this.countries; }

}
