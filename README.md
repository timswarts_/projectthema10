**Thema 10 -  - Eric Hoekstra - Version 1.0**

This repo contains the files for an interactive COVID-19 map. It will start at the homepage from where you can go to the map or an about page. When on the map a timeslider widget is available to walk trough the time. Next to that, dots will be visible for each country. These dots will be coloured according to the infection rate in this country on the selected date. This infection rate is the number of cases on that day per 100.000 inhabitants. The higher this value the more red the dot will be.


---
## COVID-19 data
The COVID-19 case data was retrieved from the Bing Covid-19 data github page: [Data link](https://github.com/microsoft/Bing-COVID-19-Data/blob/master/data/Bing-COVID19-Data.csv)  
This data can easily be updated by replacing the existing data with the new downloaded data in the resources folder.

## Dependencies
The dependencies below are configured in the build.gradle file. As long as this build.gradle file is used there should be no problem.
 
Dependencies:  
- Java version 1.8 for the backend.  
- Arcgis API version 4.18  
- JDK version 11 or higher  
- Boostrap 5 (Javascript and css files are in this repository)
- HTML5  
- Thymeleaf version 3
- Tomcat 9.0.39

## Example usage

From the homepage you can go to two different pages, the about page, and the map page. When on the map page you can do a few things. In the bottom left corner you can see the time slider. This can be used to walk trough time. In the bottom right corner a legend can be seen displaying the colour corresponding to the dots. In the top right corner there is a search bar to search for a location of your choice.   


## Set-Up

1. Clone the repo to your pc
2. Open it with your preferred UI, we would recommend IntelliJ.  
3. Create a Tomcat run configuration serving the /home url and create an artifact.  
4. Run the /home configuration.  
5. The website home page will show up in the browser.  
6. That's it, have fun!  

---

## Contact

For contact: t.swarts@st.hanze.nl or e.j.hoekstra@st.hanze.nl